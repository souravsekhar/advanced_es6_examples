'use strict';

// es5 way

const foo = function () {
	console.log('arguments object in es5', arguments); // Gets all the arguments passed
}

// es6y way

const bar = () => {
	console.log('arguments object in es6', arguments) // Doesn't get the arguments passed. Alternatively, use rest params
}

foo(1,2,3);
bar(1,2,3,4,5,6,7,8,9);