//rest operator

function argLength(...args) {
    console.log("Argument Length = "+args.length);
}
  
  argLength(3, 4, 9, 8, 6);  
  argLength(5); 
  argLength(5, 6, 7);

//console.log("**********************");

   function sortArguments(...args) {
    var sortedArgs = args.sort();
    return sortedArgs;
  }
  
   console.log("Sorted Arguments " + sortArguments(5, 3, 7, 1)); 

// console.log("**********************");

function multiply(multiplier, ...args) {
    return args.map(function(element) {
      return multiplier * element;
    });
  }
  
console.log(multiply(2, 6, 2, 5)); 

  
