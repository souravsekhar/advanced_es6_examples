'use strict';

var jackObj = {
	name: "Jack",
	printName: function () {
		var self = this;// unneccessary self assignment to handle closure gotchas
		var fullName = function () {			
			console.log('Name is', self.name);
		}
		fullName();
	}
}

var jennyObj = {
	name: 'Jenny',
	printName: () => {// 'this' is undefined here		
		var fullName = () => {
			console.log('Name is', this.name);
		}
		fullName();
	}
}

var foalObj = {
	name: 'Foal',
	printName: function () {// arrow function solves this binding issue
		var fullName = () => {
			console.log('Name is', this.name);
		}
		fullName();
	}
}

jackObj.printName();
jennyObj.printName();
foalObj.printName();