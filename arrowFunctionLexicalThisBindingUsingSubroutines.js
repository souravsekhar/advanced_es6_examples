'use strict';

var jennyObj = {
	name: 'Jenny',
	printJenny: function (arrOfNames) {
		var self = this;// unneccessary self assignment
		arrOfNames.forEach(function (names) {			
			console.log(self.name, 'is angry on', names);
		})
	}
}

jennyObj.printJenny(['Jack', 'Foal']);

var jackObj = {
	name: 'Jack',
	printJack: function (arrOfNames) {
		arrOfNames.forEach((names) => {// hurray!!! arrow function eleminated this binding			
			console.log(this.name, 'is angry on', names);
		})
	}
}

jackObj.printJack(['Jenny', 'Foal']);