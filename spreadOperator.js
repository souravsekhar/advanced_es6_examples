//concat without spread operator
var arr1 = [3, 4];
var arr2 = [1, 2, arr1, 5, 6];
console.log(arr2);

/*---------------------------*/

//concat with spread operator
var arr3 = [3, 4];
var arr4 = [1, 2, ...arr3, 5, 6];
console.log(arr4);


/*----------------------------*/

//es5 way
var arr = [32, 74, 18, 06, 10];
function max(arr) {
  return Math.max.apply(null, arr);
}
console.log(max(arr));

console.log("**********************");

//es6 way

var arr = [32, 74, 18, 06, 10];
console.log(Math.max(...arr));

console.log("**********************");

var str = "Bengaluru";
console.log(...str);



